  (* 
  Perform the insertion of a element at different position in a vector 
  - element: Integer ->  any element
  - Vector: List Integer ->  List of integer value as vector
  *)
let rec insert element vector = 
  match vector with 
  | [] -> [[element]]
  | hd::tail -> 
    let l = insert element tail in 
    let insertions = List.map (fun l -> hd::l) l in (* add head to all elements in list  of list *)
    (element::hd::tail) :: insertions;; (* ajouter l'insertion en position 0 *)
  (* 
  Perform all the permutations of element vectors 
  - Vector: List Integer ->  List of integer value as vector
  *)
let rec permutations vector = 
  match vector with
  | [] -> [[]]
  | hd::tail -> 
    let l = permutations tail in
    let perm = List.map (insert hd) l in
    List.concat perm;;


    (* 
  Compute a score of a permutation
  - perm: List Integer -> List of integer value as permutation
  - adj: 2D Array -> Adjacency matrix of weight
  *)
let score perm adj = 
  let weights = List.mapi (
      fun row column -> adj.(row).(column)
    ) perm in 
  List.fold_left (+) 0 weights;;

  (* 
  Compute the different score for each permutation
  - purmutation: List of Integer's List -> List of integer's list where a element represent a permutation
  - adjacency: 2D Array -> Adjacency matrix of weight
  *)
let scores permutations adjacency = 
  List.map (fun perm -> 
      score perm adjacency
    ) permutations;;

(* 
  Find the best permutation
  - adj: 2D Array -> Adjacency matrix of weight
  *)
let best_perms adj = 
  let perms = permutations (Core.List.range 0 (Array.length adj)) in

  let scores = scores perms adj in

  List.fold_left2 (fun (min_score, bests) perm score ->
      if score = min_score then
        (min_score, perm::bests)
      else if score < min_score then
        (score, [perm])
      else 
        (min_score, bests)) (10000, []) perms scores;;
