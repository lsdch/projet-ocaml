(* 
  Type of position object
  position: Integer -> Position
  chrom: String -> Chromosome which position object belongs to
*)
type t = {
  position: int;
  chrom: string
}

type aligned =  Pos of t | Gap
type pair = (aligned * aligned)

(* 
  Compare two position
  e1: position object -> Any position object with position feature
  e2: position object -> Any position object with position feature
*)
let compare e1 e2 = 
  let pos = compare e1.position e2.position in
  if pos = 0 then compare e1.chrom e2.chrom
  else pos

(* 
  Compute the distance between two position
  e1: abstract object -> Any position object with position feature
  e2: abstract object -> Any position object with position feature
*)
let distance e1 e2 = abs (e1.position - e2.position)

(* 
  Create a tuple of chromosome and position object associated
*)
let of_tuple (chrom, pos) = {position=pos;chrom=chrom}

  (* 
     Partition position list by chromosome 
     Result is a hashtbl of (chrom;position_list)
     Lists are unsorted. 
     position_list : List of Position object
   *)
let partition position_list = 

  List.fold_left (fun acc pos -> 
      let chrom = pos.chrom in 
      match Hashtbl.find_opt acc chrom with 
      | Some pos_list -> Hashtbl.replace acc chrom (pos::pos_list); acc
      | None -> Hashtbl.add acc chrom [pos]; acc
    ) (Hashtbl.create 0) position_list

let print elt = 
  print_string elt.chrom; 
  print_char '@'; 
  print_int elt.position;
;;

(* Print function for debugging *)
let print_pair (e1, e2) = begin
  begin match e1 with 
    |Pos p1 -> print p1;
    |Gap -> print_string " - ";
  end;
  print_string " :: ";
  begin match e2 with 
    |Pos p2 -> print p2;
    |Gap -> print_string " - ";
  end;
  print_newline();
end
