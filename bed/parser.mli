val parse_line :
  string -> 
  (string * int)

val parse_position :
  string -> 
  Position.t

val parse_bed : 
  file:string -> line_parser:(string -> 'a) -> 
  'a list

val as_dist_matrix :
  Position.t list -> Position.t list ->
  int array array