## Projet-OCamL

Verifying predictions of Transposable Element positions with the implementation of two algorithms, using the functionnal langage OCaml. 

### Getting Started

First, you can clone this project from the GitLab repository using git :

```bash
git clone http://pedago-service.univ-lyon1.fr:2325/lduchemin/projet-ocaml.git
```

This repository contains the implementations of the Hungarian algorithm and of a dynamic algorithm. The latter was especially written for solving the specific problem of position assignment between two BED files.

Please keep in mind that this project consist of a small contribution for a larger pipeline.

### Prerequisites

These implementations are written in OCaml, so you will need a running installation of these packages :

* Opam
* Ocaml
* Core
* Dune

### Installing

Once all of these tools are set up, you need to build the project from the main directory:

```bash
dune build
```

This will create two executable files.

### Running the tests

#### a) Testing the hungarian algorithm :

```bash
dune exec test_hun
```

This test makes use of a naive algorithm (based on choosing the best answer by computing every permutations possible) in order to compare its results with those of the hungarian algorithm. Both algorithms are given the same random-generated matrix. At the end of the process, the assertion "Hungarian = Perm" prompted in the standard output is here to attest that all results coincide.

#### b) Testing the dynamic algorithm :

This one takes three arguments in the command line :

* the path of two BED files
* the maximal distance treshold between two positions that you want to allow (in number of base pair).

```bash
./_build/default/test_dynamic/test_dynamic.exe <bed_1> <bed2> -d <distance_max>
```

Here's an example  :

```bash
./_build/default/test_dynamic/test_dynamic.exe -d 20000 test_dynamic/PROTOP_DmGoth6.xls  test_dynamic/PROTOP_DmGoth10.xls
```