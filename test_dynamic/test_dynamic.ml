open Dynamic
open Bed

let regular_file =
  Core.Command.Arg_type.create
    (fun filename ->
       match Core.Sys.is_file filename with
       | `Yes -> filename
       | `No | `Unknown ->
         Core.eprintf "'%s' is not a regular file.\n%!" filename;
         exit 1)

let command =
  Core.Command.basic
    ~summary:"Generate an MD5 hash of the input data"
    ~readme:(fun () -> "More detailed information")
    Core.Command.Let_syntax.(
      let%map_open
        f1 = anon ("filename" %: regular_file)
      and f2 = anon ("filename" %: regular_file)
      and dist = flag "-d" (required int) ~doc:"Max alignment distance"
      in
      (fun () -> 
         align_bed f1 f2 ~max_dist:dist
         |> Hashtbl.iter (fun _ align ->
             List.iter Position.print_pair align;
           )
      )
    );;

Core.Command.run command