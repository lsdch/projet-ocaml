(* ========== MODULES ======================================================= *)
  (* 
  Creating Vertex structure
   type partition: Char -> The partition of the vertex
   type t :
      weight : Integer -> Weight value apply on the Vertex object
      partition : Partition -> Affect the partition type to the vertex
      partition : Integer -> Unique identifier to retrieve the Vertex object
  *)
module Vertex = struct
  type partition = |A |B 
  type t = {
    mutable weight:int; 
    id:int; 
    partition: partition
  }

  (* Traducing the vertex partition into character*)
  let partition_to_char = function
    |A -> 'A'
    |B -> 'B'

  let print vertex = 
    print_int vertex.id; print_char '.';
    print_char (partition_to_char vertex.partition);
    print_string ":"; print_int vertex.weight

 (* 
 Compare two vertices to check if they are different
  - Vertex object
  - Vertex object 
  *)
  let compare v1 v2 = 
    let part_cmp = compare v1.partition v2.partition in
    if part_cmp = 0 
    then compare v1.id v2.id
    else part_cmp

end (* Module Vertex *)
(* Construction of a set of vertex objects *)
module VertexSet = struct 
  include Set.Make (Vertex)

  (* 
  Create a List of Vertex object 
  - start: Integer representing the start range of Vertex for the given partition
  - n: Integer representing end range
  - partition: Char that designate the parition of the starting graph
  *)
  let of_range start n partition = 
    List.fold_left (fun acc id -> 
        add {
          Vertex.weight=0; 
          id=id;
          partition=partition} acc
      ) empty (Core.List.range start n)

  (* 
  Update vertices weight 
  - operator: Operator from pervasives 
  - vertices: List of vertices object
  - delta: Integet -> Delta value to apply on vertices weight
  *)
  let update_potentials operator vertices delta = 
    iter(fun vertex -> 
        Vertex.(vertex.weight <- operator vertex.weight delta)
      ) vertices

  let _print vertex_set = iter (
      fun v -> Vertex.print v; print_string " ; "
    ) vertex_set

end (* Module VertexSet *)
  (* 
  Creating Edge structure
   i : Vertex Object -> Vertex on i partition: 
   j : Vertex Object -> Vertex on j partition
   orientation :Bool -> true : is part of tight_graph | false : is not part of the tight graph
   weight : Integer -> Weight value apply on the Edge object
   id : Integer -> Unique identifier to retrieve the Edge object
  *)
module Edge = struct
  type t = {
    i:Vertex.t; 
    j:Vertex.t; 
    orientation:bool; 
    weight:int; 
    id:int
  }

  let print edge = 
    let link = if edge.orientation 
      then " <--- "
      else " ---> " in
    print_int edge.id; print_string " : " ; 
    Vertex.print edge.i; print_string link; Vertex.print edge.j;
    print_string " :: "; print_int edge.weight;print_newline();;

  (* 
   Compare two edges to check if they are different 
  - e1: Edge object -> First Edge object 
  - e2: Edge object -> Second Edge object
  *)
  let compare e1 e2 = compare e1.id e2.id

  (* 
   Compute the delta by the given formula: Eight Weight - (Vertice weight on i partition - Vertice weight on j partition) 
  - edge: Edge object
  *)
  let delta edge = edge.weight - Vertex.(edge.i.weight + edge.j.weight)


  (* 
  Check if an Edge object is part or not of the tight graph
  - edge: Edge object
  *)
  let is_tight edge = 
    Vertex.(edge.i.weight + edge.j.weight) = edge.weight

  (* 
  Reverse the orientation for a given Edge object
  - edge: Edge object
  *)
  let reversed edge = {edge with orientation = not edge.orientation}

  (* 
  Check if two edge are connected and form a path 
  - edge1 : Edge object -> First edge 
  - edge2 : Edge object -> Second edge
  *)
  let are_path edge1 edge2=(((edge1.j = edge2.j) && edge2.orientation) || 
                            ((edge1.i = edge2.i) && edge1.orientation)) && 
                           edge1.orientation != edge2.orientation

end (* Module Edge *)
  (* 
  Construct an edge set of Edge objects
  *)
module EdgeSet  = struct
  include Set.Make (Edge)

  let _print = iter Edge.print

  (* 
    Create vertices set of Vertex objects by extracting all vertices from a given edge set
     - edge_set : Edge set object
  *)
  let vertices edge_set = fold (fun {Edge.i ; j ; _} acc -> 
      acc
      |> VertexSet.add i
      |> VertexSet.add j
    ) edge_set VertexSet.empty


end (* Module EdgeSet *)
(* 
  Create a tight graph structure with some functions
*)
module TightGraph = struct 

(*
  The output of a Tight Graph structure give:
  - weight: Integer -> Total weightof the matching
  - edges : List of edges id tuple -> Return different couples of edges representing containing in the matching
*)
  type output = {
    weight: int;
    edges: (int*int) list
  }

(*
  Required parameters to construct the Tight Graph
  - sources: Vertex set from source partition
  - targets: Vertex set from target partition
  - vertices: Vertex set containing all the vertices 
  - edges: Edge Set containing all the edges
  - edge_pool : All edges that may be added to the Tight Graph but not necessary part of 
  - matching : Edge set of all the edges included in the matching 
*)
  type t = {
    sources: VertexSet.t;
    targets: VertexSet.t;
    vertices: VertexSet.t;
    mutable edges: EdgeSet.t;
    edge_pool: EdgeSet.t;
    matching: EdgeSet.t;
  }

  (*
    Update the total weight the edges in the matching for the output
    - tight_graph: Tight Graph object 
  *)
  let as_list tight_graph = 
    EdgeSet.fold (
      fun edge matching -> {
          weight = matching.weight + edge.weight;
          edges = (edge.i.id, edge.j.id)::matching.edges}
    ) tight_graph.matching {weight=0;edges=[]}
  (* |> List.sort (fun (a, _) (b, _) -> compare a b)  *)
  (*
    Initalise vertices Tight Graph with a adjacency matrix
    - matrix: 2D Array -> adjacency matrix 
  *)
  let init_vertices matrix = 
    let n = Array.length(matrix) in
    let m = Array.length(matrix.(0)) in
    let v_left = VertexSet.of_range 0 n Vertex.A in
    let v_right = VertexSet.of_range 0 m Vertex.B in
    (v_left, v_right)

  (*
    Initalise edges Tight Graph with vertices sets and adjacency matrix
    - v_let: Vertex Set -> Left vertices partition
    - v_right: Vertex Set -> Right vertices partition
    - weights: 2D Array -> Adjacency Matrix
  *)
  let init_edges v_left v_right weights=
    VertexSet.fold (fun v1 acc -> 
        VertexSet.fold (fun v2 acc -> 
            EdgeSet.add ({Edge.i=v1; 
                          j=v2; 
                          orientation=false; 
                          weight=weights.(v1.Vertex.id).(v2.Vertex.id);
                          id=v1.Vertex.id*(VertexSet.cardinal v_right)+v2.Vertex.id
                         }) acc
          ) v_right acc
      ) v_left EdgeSet.empty

  (*
    Initalise potentials with the minimum weight for each right vertices of Tight Graph 
    - v_right: Vertex Set -> Right vertices partition
    - weights: 2D Array -> Adjacency Matrix of weights
  *)
  let init_potentials v_right weights =
    let transWeights = Core.Array.transpose_exn weights in
    VertexSet.iter (fun vertex -> 
        let min_weight = 
          Core.Array.min_elt transWeights.(vertex.Vertex.id) ~compare:Core.Int.compare in
        match min_weight with
          Some min_weight -> vertex.Vertex.weight <- min_weight
        | None -> raise (Invalid_argument "No minimum found in adjacents ??")
      ) v_right

  let init_tight_edges = EdgeSet.filter Edge.is_tight

  (*
    Normalize adjacency matrix
    - matrix: 2D Array -> Adjacency matrix
  *)
  let normalize matrix =
    let min_weights = Array.map 
        (fun row -> 
           let m = Core.Array.min_elt row ~compare in 
           match m with
             Some m -> m
           | None -> raise (Invalid_argument "No minimum found in row")
        ) matrix in
    let min_weight = Core.Array.min_elt min_weights ~compare in
    match min_weight with 
      Some min_weight ->
      if min_weight < 0 then
        Array.map (fun row -> 
            Array.map (fun elt -> elt - min_weight) row
          ) matrix
      else matrix
    |None -> raise (Invalid_argument "No minimum found in matrix")
  
  
  (*
    Recreate Edges and Vertices sets of Tight Graph with a normalized matrix
    - matrix: 2D Array -> Adjacency matrix
  *)
  let of_weight_matrix matrix = 
    let normalized = normalize matrix in
    let (v_left, v_right) = init_vertices normalized in
    init_potentials v_right matrix;
    let edges = init_edges v_left v_right normalized in
    {
      sources = v_left;
      targets = v_right;
      vertices = VertexSet.union v_left v_right;
      edges = (init_tight_edges edges);
      edge_pool = edges;
      matching = EdgeSet.empty
    }

  (* ============= Pathfinding ============================================= *)
  (*
    Check if an edge is branched to another and if it's not already seen
    - edge_set: Edge Set -> An Edge set of the Tight Graph
    - start_edge: Edge object -> Any edge involved in the Tight Graph as starting point
    - seen: Vertex Set -> All vertices already seen during the pathfiding
  *)
  let branching edge_set start_edge seen =
    EdgeSet.filter (fun edge -> 
        Edge.are_path start_edge edge && 
        not (VertexSet.mem edge.i seen && VertexSet.mem edge.j seen)
      ) edge_set

  (*
    Depth-first search function
    - edge_set: Edge Set -> An Edge set of the Tight Graph
    - start_edge: Edge object -> Any edge involved in the Tight Graph as starting point
    - seen: Vertex Set -> All vertices already seen during the pathfiding
  *)
  let rec dfs edge_set start_edge seen = 
    let seen = seen
               |> VertexSet.add start_edge.Edge.i
               |> VertexSet.add start_edge.Edge.j
    in 
    let branch = branching edge_set start_edge seen  in
    EdgeSet.fold (
      fun edge acc -> dfs (EdgeSet.remove start_edge edge_set) edge acc
    ) branch seen 

  (*
    Retrieve paths from a starting edge to targets
    - targets: Vertex Set -> Set of target-partition vertices 
    - start_edge: Edge object -> Any edge involved in the Tight Graph as starting point
    - path: List of edge objects -> The edges constituting the path
  *)
  let rec pathfinder targets start_edge edge_set path= 
    let edge_set = (EdgeSet.remove start_edge edge_set) in
    if VertexSet.mem start_edge.j targets && not start_edge.orientation 
    then Some (start_edge::path)
    else 
      let branches = EdgeSet.filter (
          fun edge -> Edge.are_path start_edge edge
        ) edge_set in
      EdgeSet.fold (fun edge acc -> match acc with 
          |None -> pathfinder targets edge edge_set (start_edge::path)  
          |Some acc -> Some acc) branches None

  (*
    Retrieve path from source-partition vertices to target-partition vertices
    - sources: Vertex Set -> Set of source-partition vertices 
    - targets: Vertex Set -> Set of target-partition vertices 
    - edge_set: Edge Set -> An edge set of the Tight Graph
  *)
  let path_from_to sources targets edge_set =
    let start_edges = EdgeSet.filter (fun edge -> 
        VertexSet.mem edge.i sources && not edge.orientation
      ) edge_set in 
    let path = EdgeSet.fold (fun start acc -> match acc with 
        |None -> pathfinder targets start edge_set []
        |Some acc -> Some acc) start_edges None in
    match path with 
    |Some path -> path
    |None -> invalid_arg "No valid path found"

  (*
    Fetch all reachable vertices from given starting edges
    - vertices: Vertex Set -> Set of vertices involved in the Tight Graph 
    - tight_graph: Tight Graph object
  *)
  let reachable_from vertices tight_graph =
    let start_edges = EdgeSet.filter (fun edge -> 
        not edge.orientation && VertexSet.mem edge.i vertices 
      ) tight_graph.edges in
    EdgeSet.fold (fun start_edge acc  ->
        if (VertexSet.mem start_edge.Edge.i vertices && 
            not start_edge.Edge.orientation)
        then dfs tight_graph.edges start_edge acc
        else acc)  start_edges VertexSet.empty


  (* ============= Matching ================================================ *)
  (*
    Get the edges involved in the matching of the Tight Graph
    - tight_graph: Tight Graph object
  *)
  let matching tight_graph = 
    EdgeSet.filter (fun edge -> edge.Edge.orientation) tight_graph.edges

  (*
    Check if a perfect matching exist in the Tight Graph
    - tight_graph: Tight Graph object 
  *)
  let is_perfect_matching tight_graph=
    let matched_vertices = EdgeSet.vertices tight_graph.matching in
    VertexSet.equal matched_vertices tight_graph.vertices

  (* let matching_weight matching_edges =
     EdgeSet.fold (fun edge acc -> acc + edge.weight) matching_edges 0 *)
  (* ============= Hungarian blood magic =================================== *)
  (*
    Compute reachable vertices from source (Rs) & target (Rt) partition
    - tight_graph: Tight Graph object
  *)
  let compute_r tight_graph  = 
    let open Edge in
    EdgeSet.fold (
      fun edge (rs,rt) -> if edge.orientation  
        then (VertexSet.remove edge.i rs, VertexSet.remove edge.Edge.j rt)
        else (rs,rt)
    ) tight_graph.edges (tight_graph.sources, tight_graph.targets) 

  (*
    Check if an edge is sharing It's vertices between a right and a left set
    - left_set: Vertex Set -> Left vertex set
    - right_set: Vertex Set -> Right vertex set
  *)
  let is_linking_sets (edge: Edge.t) left_set right_set = 
    (VertexSet.mem edge.Edge.i left_set) && (VertexSet.mem edge.Edge.j right_set)

  (*
    Compute and erturn the delta value
    - z_inter_s: Vertex Set -> Vertex set of reachable vertices involved in source-partition 
    - t_bar_z: Vertex Set -> Vertex set of not reachable vertices involved in target-partition 
    - edges : Edge set -> Edge set from tight graph
  *)
  let compute_deltas z_inter_s t_bar_z edges = 
    EdgeSet.fold (fun (edge: Edge.t) acc -> 
        if  is_linking_sets edge z_inter_s t_bar_z 
        then (Edge.delta edge)::acc
        else acc )
      edges [] 


  (*
    Reverse a path provided from the Tight Graph
      - tight_graph: Tight Graph object
      - pat: List edge object -> Path provided from the Tight Graph
  *)

  let reverse_edges_in_path tight_graph path = 
    EdgeSet.map (fun edge -> if List.mem edge path
                  then (Edge.reversed edge)
                  else edge) tight_graph.edges

  (*
    Update the Tight Graph by adding and removing tight edges from the Tight Graph
      - tight_graph: Tight Graph object
  *)
  let update_tight_edges tight_graph= 
    EdgeSet.union tight_graph.edges (EdgeSet.filter Edge.is_tight tight_graph.edge_pool)

  (*
    Core of the Hungurian Algorithm - Follow the Hungurian algorithm steps
      - tight_graph: Tight Graph object
  *)
  let rec hungarian_iter tight_graph =
    let tight_graph = {tight_graph with matching = matching tight_graph} in
    if is_perfect_matching tight_graph
    then tight_graph
    else
      let (r_s, r_t) =  compute_r tight_graph in
      let z = VertexSet.union r_s (reachable_from r_s tight_graph) in
      (* Z empty ? switch deltas : invert path  *)
      if VertexSet.is_empty (VertexSet.inter r_t z) 

      then
        let (z_inter_s, t_bar_z, z_inter_t) = (
          VertexSet.inter z tight_graph.sources, 
          VertexSet.diff tight_graph.targets z, 
          VertexSet.inter z tight_graph.targets
        ) in
        let deltas = compute_deltas z_inter_s t_bar_z tight_graph.edge_pool in
        let min_delta = Core.List.min_elt deltas ~compare:Core.Int.compare in 
        begin match min_delta with 
          | Some min_delta ->
            assert (min_delta <> 0);
            VertexSet.update_potentials (+) z_inter_s min_delta;
            VertexSet.update_potentials (-) z_inter_t min_delta;

            (* Update tight graph with potential new tight edges *)
            hungarian_iter {
              tight_graph with 
              edges = update_tight_edges tight_graph
            }
          |None -> raise (Invalid_argument "No minimum delta was found")
        end

      else 
        let inverting_path = path_from_to r_s r_t tight_graph.edges in
        (* Update tight graph by reversing a path from Rs to Rt *)
        hungarian_iter {
          tight_graph with 
          edges = reverse_edges_in_path tight_graph inverting_path
        }



end (* module TightGraph *)
(*
  Execute the hungurian algorithm by providing a adjacency matrix of weight
  weights: 2D Array -> adjacency matrix of weight
*)
let hungarian weights =
  TightGraph.of_weight_matrix weights
  |> TightGraph.hungarian_iter
  |> TightGraph.as_list