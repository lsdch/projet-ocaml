module TightGraph : sig
  type t
  type output = {
    weight: int;
    edges: (int*int) list
  }
  val of_weight_matrix :
    int array array ->
    t 
  val normalize :
    int array array ->
    int array array
end

val hungarian :
  int array array ->
  TightGraph.output

