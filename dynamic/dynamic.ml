open Bed 

(* 
  Creating Align structure
   type trace: String -> Trace type
   type score :
      value : Integer -> Value of the align object
      trace : trace -> Trace type affected to align object
  *)
module Align = struct
  type trace = Ins | Del | Match 
  type score = {
    value: int;
    trace: trace;
  }

(*
    Find the optimal offset for sequence A
    - p1: Position.t list -> sequence A
    - p2: List -> sequence B
    - i: Integer -> current index in A
    - j: Integer -> current index in B
    - max_dist: -> 
  *)
  let min_elt ~compare l =
    match Core.List.min_elt l ~compare with
    | None -> assert false
    | Some s -> s

  (*
    Find the optimal offset for sequence A
    - p1: Position.t list -> sequence A
    - p2: List -> sequence B
    - i: Integer -> current index in A
    - j: Integer -> current index in B
    - max_dist: -> 
  *)
  let rec find_offset p1 p2 i j max_dist =
    if i <= 0 then i +1
    else if Position.distance p1.(i) p2.(j) <= max_dist then i+1
    else find_offset p1 p2 (i-1) j max_dist

    (*
    Open-recursive score fonction, to be memoized 
    - p1: Position.t list -> sequence A
    - p2: Position.t list -> sequence B
    - max_dist: -> maximum distance allows for alignment
    - score_rec: -> memoized self
    - x: -> current alignment index relative to A
    - y: -> current alignment index relative to B
  *)
  let open_score p1 p2 max_dist score_rec x y =
    if x = 0 then
      {value=0;trace=Del}
    else if y = 0 then
      {value=0;trace=Ins}
    else
      let i,j = (x-1, y-1) in
      if Position.distance p1.(i) p2.(j) > max_dist then
        if Position.(p1.(i).position > p2.(j).position) then
          let new_x = find_offset p1 p2 (i-1) j max_dist in
          {value=(score_rec new_x y).value;
           trace=Ins}
        else 
          let new_y = find_offset p2 p1 (j-1) i max_dist in
          {value=(score_rec x new_y ).value;
           trace=Del}
      else 
        min_elt [
          {value=(score_rec (x-1) y).value;
           trace=Ins};
          {value=(score_rec x (y-1)).value;
           trace=Del};
          {value=(score_rec (x-1) (y-1)).value +
                 (Position.distance p1.(i) p2.(j) - max_dist);
           trace=Match}
        ] ~compare:(fun t1 t2 -> compare t1.value t2.value)

  (*
    Perform the memoization feature : wraps open-recursive ff 
    and ties the recursive knot
    - ff -> Given function 
  *)
  let memo_rec ff =
    let h = Hashtbl.create 0 in 
    let rec f x y  = 
      try Hashtbl.find h (x,y)
      with Not_found ->
        let v = ff f x y in 
        Hashtbl.add h (x,y) v;
        v
    in f

    (*
      Perform 
    - p1: Position.t list -> sequence A
    - p2: Position.t list -> sequence B
    - score_fun: -> memoized score function
    - i: -> current index in A
    - j: -> current index in B
    - ali: -> accumulator for result 
  *)
  let rec align p1 p2 score_fun i j ali  =
    if i = 0 && j = 0 then
      ali
    else 
      let score = score_fun i j in
      begin match score.trace with 
        |Ins -> let pair = Position.(Pos p1.(i-1) , Gap) in 
          align p1 p2  score_fun (i-1) j (pair::ali)
        |Del -> let pair = Position.(Gap, Pos p2.(j-1)) in 
          align p1 p2 score_fun i (j-1) (pair::ali)
        |Match -> let pair = Position.(Pos p1.(i-1), Pos p2.(j-1)) in 
          align p1 p2 score_fun (i-1)(j-1) (pair::ali)
      end
end

  (*
    Perform  
    - p1: Position.t list -> sequence A
    - p2: Position.t list -> sequence B
  *)
let align_array p1 p2 ~max_dist = 
  Array.sort Position.compare p1;
  Array.sort Position.compare p2;
  let memo_score = Align.open_score p1 p2 max_dist |> Align.memo_rec in 
  Align.align p1 p2 memo_score (Array.length p1) (Array.length p2) []

  (*
    Perform the alignement of two list considering a max distance
    - p1: Position.t list -> sequence A
    - p2: Position.t list -> sequence B
  *)
let align_list p1 p2 ~max_dist =
  align_array (Array.of_list p1) (Array.of_list p2) ~max_dist:max_dist

  (*
    Perform the alignement
    - file1: String -> First Bed file provided
    - file2: String -> Second Bed file provided
  *)
let align_bed file1 file2 ~max_dist = 
  let p1_tbl = Parser.parse_bed ~file:file1 ~line_parser:Parser.parse_position
               |> Position.partition in
  let p2_tbl = Parser.parse_bed ~file:file2 ~line_parser:Parser.parse_position 
               |> Position.partition in
  Hashtbl.create (Hashtbl.length p1_tbl)
  |> Hashtbl.fold (fun chrom p1_list results -> 
      match Hashtbl.find_opt p2_tbl chrom with 
      |Some p2_list -> 
        align_list p1_list p2_list ~max_dist:max_dist
        |> Hashtbl.add results chrom;
        results
      |None -> results
    ) p1_tbl 
