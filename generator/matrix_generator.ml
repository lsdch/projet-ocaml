  (* 
  Creating a dummy matrix of weight
   n : Integer -> Number of occurence in the matrix
   min_weight : Integer -> Minimal Weight range
   max_weight : Integer -> Maximal Weight range
  *)
let make_instance n min_weight max_weight= 
  Array.map (fun _ ->
  Array.init n (fun _ -> (Random.int (max_weight-min_weight)) + min_weight)
   ) (Array.of_list (Core.List.range 0 n))