open Hungarian
open TightGraph
open Permutations


(* ===================  MAIN ===============================================  *)
(* Test instances *)
let _weights = [|[|1;2;0|];
                 [|5;2;4|];
                 [|2;8;3|]|];;
let _weights = [|[|6;1;9|];
                 [|7;0;5|];
                 [|0;0;3|]|];;
(* let weights =[|[|1; 6; 4|]; [|3; 5; 7|]; [|0; 0; 6|]|];; *)
let weights =  Matrix_generator.make_instance 8 0 10;; 

(* Tight graph initialisation *)
let tight_graph = TightGraph.of_weight_matrix weights;;
(* EdgeSet.print tight_graph.edge_pool;; *)
let result = hungarian weights;;
(* EdgeSet.elements result.edges;; *)
let (pweight, perms) = best_perms (TightGraph.normalize weights);;
(* let hweight = TightGraph.matching_weight result.matching;; *)
assert (pweight = result.weight);
print_endline "Hungarian = Perm";
(* List.iter (fun (weight,_)  -> assert (weight = TightGraph.matching_weight result.matching)) perms;; *)


(* align_bed "PROTOP_DmGoth6.xls" "PROTOP_DmGoth10.xls" ~max_dist:500
   |> Hashtbl.iter (fun _ align -> 
   List.iter Position.print_pair align
   );; *)
(* let p1_list = 
   Parser.parse_bed ~file:"PROTOP_DmGoth6.xls" ~line_parser:(Parser.parse_position) 
   |> Position.partition in  
   let p2_list = 
   Parser.parse_bed ~file:"PROTOP_DmGoth10.xls" ~line_parser:(Parser.parse_position) 
   |> Position.partition in
   align_list (Hashtbl.find p1_list "chrX") (Hashtbl.find p2_list "chrX") ~max_dist:500
   |> List.iter Position.print_pair;; *)