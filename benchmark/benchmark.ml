open Bed
open Dynamic

let generate n size= 
  List.init n (fun _ -> Position.({
      chrom="dummy";
      position=Random.int size;
    }))



(* 
let () = Random.init 0;
  List.iter Position.print (generate 5 uniform_generator 0.2);; *)
type runstat = {
  time: float;
  n: int;
  size: int;
  max_dist: int;
}

let print_stat stat = 
  print_int stat.size;
  print_char '\t';
  print_int stat.n;
  print_char '\t';
  print_int stat.max_dist;
  print_char '\t';
  print_float stat.time;
  print_newline();
;;

let run n size max_dist  = 

  let a = generate n size in 
  let b = generate n size in 
  let t = Sys.time() in
  let _ = align_list a b ~max_dist:max_dist in
  let time = Sys.time() -. t in
  { 
    time=time;
    size=size;
    n=n;
    max_dist=max_dist
  };;


let benchmark n_range size fixed = 
  let res = Hashtbl.create (List.length n_range) in 
  let size_range = match fixed with
    |true -> let base = List.hd n_range in 
      List.map (fun n -> let s = n/base*size in 
        int_of_float (float_of_int s *. 0.8)) n_range
    |false -> List.init (List.length n_range) (fun _ -> size)
  in
  List.fold_left2 (fun acc n size ->
      let dist = 20000 in
      (* size/n in *)
      let stat =  run n size dist in
      print_stat stat;
      Hashtbl.add acc (n, dist, size) stat;
      acc
    ) res n_range size_range ;;


let n_range = 
  Core.List.range 2 5
  |> List.fold_left (fun acc size -> 
      let size = float_of_int size in
      let size_inf = int_of_float (10. ** size) in
      let size_sup =int_of_float (10. ** (size+.1.)) in 
      acc@(Core.List.range size_inf size_sup ~stride:size_inf)
    ) [] in

benchmark n_range 10000000 false
(* |> Hashtbl.iter (fun _ stat ->
    print_stat stat
   );; *)